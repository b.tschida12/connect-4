#include "connect4.h"
#include "connect4fun.c"

int main(void)
{
    printf("Checking Dependencies\n");
    if(can_run_command(dependency)){
        printf("Dependencies are installed\n");
        printf("Press enter key to continue ");
        fgetc(stdin);
        
    }
    else {
        printf("%s is not installed\n", dependency);
        printf("Please install %s to play connect 4\n", dependency);
        exit(0);
    }

    system("clear");
    connect4Menu();
    
    return 0;
}
