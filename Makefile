# build connect 4 game

# Compiler specification
CC = gcc

# Compiler Flags
# -g add depugging
# -Wall turn on compile warnings
CFLAGS = -g -Wall

# File linking

# Build target
TARGET = connect4

# Source Files
SRC_FILES = connect4.c connect4fun.c connect4.h
all: $(TARGET)

# Build connect4
$(TARGET): connect4.c connect4fun.c connect4.h
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c