/*
    Game of connect 4
        - Players take turns placing tokens, first to get 4 in a row wins
        - Can be in a row vertical, horizontal, or diagonal
        - Can change the size of the board and quantity to win using
            ROW, COL, WIN
*/
#ifndef CONNECT4_H
#define CONNECT4_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>

// ROW = total number of rows for game
// COL = total number of columns for game
// WIN = number of tokens in a row to win game
#define ROW 6
#define COL 7
#define WIN 4


// Defined globally player token
char player[2];

char dependency[] = "cowsay";

// board.occ = true when token in place
// color = R for red, B for black

struct Board {
    bool occ[ROW][COL];
    char color[ROW][COL];
} board;

void connect4Menu(void);

// sets board.occ to 0,
// sets board.color to \0
void initBoard(void);

// displays board w/ rows and row & column numbers
void printBoard(void);

// Play game of connect 4, holds all functions
void game(void);

// Places token for given player
void placeToken(int col, char color);

// Determines if game is over
int isWin(void);

// Gets token for each player
void getToken(void);

// Checks if program is installed
bool can_run_command(const char *cmd);

#endif