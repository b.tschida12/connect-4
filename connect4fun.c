#include "connect4.h"

// checks if dependencies are installed
bool can_run_command(const char *cmd) {
    if(strchr(cmd, '/')) {
        // if cmd includes a slash, no path search must be performed,
        // go straight to checking if it's executable
        return access(cmd, X_OK)==0;
    }
    const char *path = getenv("PATH");
    if(!path) return false; // something is horribly wrong...
    // we are sure we won't need a buffer any longer
    char *buf = malloc(strlen(path)+strlen(cmd)+3);
    if(!buf) return false; // actually useless, see comment
    // loop as long as we have stuff to examine in path
    for(; *path; ++path) {
        // start from the beginning of the buffer
        char *p = buf;
        // copy in buf the current path element
        for(; *path && *path!=':'; ++path,++p) {
            *p = *path;
        }
        // empty path entries are treated like "."
        if(p==buf) *p++='.';
        // slash and command name
        if(p[-1]!='/') *p++='/';
        strcpy(p, cmd);
        // check if we can execute it
        if(access(buf, X_OK)==0) {
            free(buf);
            return true;
        }
        // quit at last cycle
        if(!*path) break;
    }
    // not found
    free(buf);
    return false;
}

void game(void)
{
    int choice, game = 0;
    int count = 1;
    char player_cur;

    initBoard();

    getToken();

    system("clear");
    
    while (game == 0)
    {
        // Alternates player 
        if (count % 2 == 0){
            player_cur = player[0];
            printf("It is Player 1's turn.\n");
        }
        else {
            player_cur = player[1];
            printf("It is Player 2's turn.\n");
        }

        printBoard();
        printf("Enter your column: ");

        do{
            scanf("%d", &choice);
            if (board.occ[0][choice - 1] == true)
            {
                printf("column is full\n");
            }
            if (choice > COL || choice < 1){
                printf("Invalid choice. try again: ");
            }
        } while (choice < 1 || choice > COL || board.occ[0][choice - 1] == true);

        placeToken(choice - 1, player_cur); // player enters 1-7, array from 0-6
        game = isWin();
        count++;
        system("clear");
    }

    printBoard();

    if (game == 1)
    {
        printf("Player1 (%c) won!", player[0]);
    }
    else if (game == -1)
    {
        printf("Player2 (%c) won!", player[1]);
    }
    else if (game == 2)
    {
        printf("Its a tie");
    }
}

void connect4Menu(void)
{
    int choice;
    FILE *rules;

    printf("Connect 4 Main menu\n");
    printf("1 - Play Connect 4\n");
    printf("2 - Read Rules\n");
    printf("3 - Quit game\n");
    printf("Enter choice: ");

    // Input verification
    do{
        scanf("%d", &choice);
        if(choice < 1 || choice > 3){
            printf("Try again\n");
        }
    } while (choice < 1 || choice > 3);

    switch(choice)
    {
        case 1:
            game();
            printf("\nPress enter key to return to main menu");
            fgetc(stdin);
            fgetc(stdin);
            system("clear");
            connect4Menu();
        case 2:
            rules = fopen("rules.txt", "r");
            if(rules == NULL){
                printf("Cannot open rules.txt\n");
                exit(0);
            }
            else {
                system("clear");
                system("cat rules.txt | cowsay -n");
                fclose(rules);
                printf("\n\n");
                printf("Press any key to return to main menu ");
                getc(stdin);
                fgetc(stdin);
                system("clear");
                connect4Menu();
            }
            fclose(rules);
        case 3:
            printf("Now exiting");
            exit(0);
        default:
            break;
        }
}

void getToken(void)
{
    for (int i = 0; i < 2; i++){
        printf("Player%d, enter a token: ", i+1);
        do{
            scanf(" %c", &player[i]);
            if(i > 0){
                if (player[i] == player[i-1]){
                    printf("Token already chosen, pick a different one: ");
                }
            }

        } while (player[i] == player[i-1]);
    }
}

// return 1 if p1 wins, -1 for p2, 0 if neither, 2 for tie
int isWin(void)
{
    int p1 = 0, p2 = 0, k = 0;
    int i =0, j = 0;
    int tie = 0;

    // if top row all has tokens, game is a tie
    for (i = 0; i < ROW; i++){
        if (board.occ[i][COL - 1]){
            tie++;
        }
    }
    if (tie == ROW ){
        return 2;
    }

    // Check Horizontal for 4 in a row
    for (i = 0; i < ROW; i++) // Start at row 0, iterate to last row
    {
        // Start column = 0, iterate until col + # to win
        for (j = 0; j < COL - WIN; j++){
            // starts where J is, goes until J + WIN
            p1 = 0;
            p2 = 0;
            for (k = j; k < j + WIN; k++)
            {
                // only checks when begining has a token in it
                if (!board.occ[i][k]){break;}
                else{
                    // Counts what color is in position, 
                    if (board.color[i][k] == player[0]){
                        p1++;
                    }
                    else if (board.color[i][k] == player[1]){
                        p2++;
                    }
                }
            }
            if (p1 == 4){
                return 1;
            }
            else if (p2 == 4){
                return -1;
            }
        }
    }
    
    // Check for vertical in a row
    for (i = 0; i < COL; i++){
        for (j = 0; j < ROW + 1 - WIN; j++){
            p1 = 0;
            p2 = 0;
            for (k = j; k < j + WIN; k++)
            {
                if (!board.occ[k][i]){
                    break;
                }
                else {
                    if (board.color[k][i] == player[0]){
                        p1++;
                    }
                    else if(board.color[k][i] == player[1]){
                        p2++;
                    }
                }
                if (p1 == 4){
                    return 1;
                }
                else if (p2 == 4){
                    return -1;
                }
            }
        }
    }

    // Check for Diagonal in a row
    // start at 0,0 -> Check 0,0 1,1 2,2 3,3
    // go to 0,1 -> Check 0,1-1,2-2,3-3,4
    // etc
    for(i = 0; i < ROW - WIN; i++){
        for (j = 0; j < COL - WIN; j++){
            p1 = 0;
            p2 = 0;
            // modifier to iterate diagonally
            for (k = 0; k < WIN; k++){
                if(!board.occ[i+k][j+k]){
                    break;
                }
                else if(board.color[i+k][j+k] == player[0]){
                    // Player 1 has token at location
                    p1++;
                }
                else{
                    p2++;
                }
            }
            if (p1 == 4){
                return 1;
            }
            else if(p2 == 4){
                return -1;
            }
        }
    }

    // check diagonal opposite direction
    // CURRENTLY NOT WORKING PROPERLY
    for (i = ROW - 1; i >= WIN - 1; i--){
        for (j = COL - 1; j >= WIN - 1; j--){
           p1 = 0;
           p2 = 0;
            for (k = 0; k < WIN; k++){
                if (!board.occ[i - k][j - k]){
                    break;
                }
                else if(board.color[i-k][j-k] == player[0]){
                    // Player 1 has token at location
                    p1++;
                }
                else{
                    p2++;
                }
            }
            if (p1 == 4){
                return 1;
            }
            else if(p2 == 4){
                return -1;
            }
        }
    }

    return 0;
}

void placeToken(int col, char color)
{
    // Fills from row 0 to top row
    // ie: if rows 0-3 full, will place in 4
    for (int i = ROW - 1; i >= 0; i--){
        if(!board.occ[i][col]) {
            board.occ[i][col] = true;
            board.color[i][col] = color;
            break;
        }
    }
}

void initBoard(void)
{
    for (int i = 0; i < ROW; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            board.color[i][j] = ' ';
            board.occ[i][j] = false;
        }
    }
    for (int i = 0; i < 2; i++){
        player[i] = '\0';
    }
}

void printBoard(void)
{
    printf("\n|");
    for (int i = 1; i <= COL; i++)
    {
        printf("%d|", i);
    }
    printf("\n");
    for (int i = 0; i < ROW; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            printf(" ");
            if (!board.occ[i][j])
            {
                printf("-");
            }
            else
            {
                printf("%c", board.color[i][j]);
            }
        }
        printf(" \n");
    }
}